import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  submitForm(dataObj)
  {
    // console.log(dataObj);
    if(dataObj.username=="admin"&&dataObj.password=="admin")
    {
      //navigate to admindashboard component
      this.router.navigate(['admindashboard']);
    }
    else
    {
      //navigate to userdashboard component
      this.router.navigate(['userdashboard']);
    } 
  }
}
