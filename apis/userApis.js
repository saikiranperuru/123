const exp = require("express");
const userApp = exp.Router();


// localhost:port/user/login(POST)
// localhost:port/user/register(POST)
// localhost:port/user/readprofile/username(GET)

const dbo=require('../db');
dbo.initDb();

userApp.get('/readprofile/:username',(req,res)=>{
    res.send({message:"user profile works"});
});

userApp.post('/login',(req,res)=>{
    res.send({message:"user login works"});
});

userApp.post('/register',(req,res)=>{
    res.send({message:"user  works"});
});

//export userApp
module.exports=userApp;