//create mini express app to handle admin reqs
const exp = require("express");
const adminApp = exp.Router();


// localhost:port/admin/login(POST)
// localhost:port/admin/readprofile/username(GET)


const dbo=require('../db');
dbo.initDb();

adminApp.get('/readprofile/:username',(req,res)=>{
    res.send({message:"admin profile works"});
});

adminApp.post('/login',(req,res)=>{
    res.send({message:"admin login works"});
});

//export  adminApp
module.exports=adminApp;