//inport and install mongodb
const mc = require("mongodb").MongoClient;

//import 
var dbo;
//get database url
const dbUrl = "mongodb+srv://vnr:vnr@cluster0-d804y.mongodb.net/test?retryWrites=true&w=majority";

function initDb() {
    mc.connect(dbUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, (err, client) => {
        if (err) {
            console.log("err during db conn", err);
        } else {
            dbo = client.db("mydb");
            console.log("connected to db..");
        }
    });
}

function getDb() {
    console.log(dbo, "Db has not been initialised. Please called initDb");
    return {
        dbo: dbo,
    }
}

module.exports = {
    getDb,
    initDb
}